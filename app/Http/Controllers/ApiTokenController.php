<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\User;
use App\Item;
use Illuminate\Http\Request;

class ApiTokenController extends Controller
{
    /**
     * Update the authenticated user's API token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function update(User $user)
    {
        $token = Str::random(60);
        $user->api_token = hash('sha256', $token);
        $user->save();
        return ['token' => $token];
    }

    public function getItems()
    {
        $items = Item::all();
        $array = [];
        foreach ($items as $item) {
            $array[$item->name] = $item->subItems;
        }
        return $array;
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubItem extends Model
{
    protected $table = 'sub_items';

    public function itemDetails()
    {
        return $this->hasMany(ItemDetail::class, 'sub_items_id');
    }
}

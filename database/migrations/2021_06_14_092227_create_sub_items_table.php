<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('item_id');
        });

        $data = [
            ['name'=>'SAFEGUARD', 'item_id' => 1,],
            ['name'=>'DOVE', 'item_id' => 1,],
            ['name'=>'COLGATE', 'item_id' => 2,],
            ['name'=>'CLOSE UP', 'item_id' => 2,],
            ['name'=>'PANTENE', 'item_id' => 3,],
            ['name'=>'SUNSILK', 'item_id' => 3,],
        ];

        DB::table('sub_items')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_items');
    }
}

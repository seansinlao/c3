<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function subItems()
    {
        return $this->hasMany(SubItem::class);
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   {{ __('You are logged in!') }}
                </div>
<hr>
                <input type='hidden' id='userId' value="{{Auth::user()->id}}">
                <button type="button" class='btn btn-primary' id="btnToken">Press to Generate Token</button>
                <br>
                <center><span id="apiToken">Generated token will be displayed here.</span></center>
                <br>

                <button type="button" class='btn btn-secondary' id="btnTree">SHOW CHECKBOX TREE</button>
                <hr>
                <div id="treeContent" class="d-none">
                    <div class="collapse-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        {{-- SOAP --}}
                        @foreach ($items as $item)
                        <div class="card">
                            <div class="card-header" role="tab" id="heading-{{$item->id}}">
                            <h5 class="mb-0">
                                <a class="d-block" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$item->id}}" aria-expanded="true" aria-controls="collapse-{{$item->id}}">
                                {{$item->name}}
                                <span class="float-right">Select All <input type="checkbox" id="sa_foo_{{$item->id}}" value='0' name="sa_foo" data-checkbox-name="foo" class="selectall-{{$item->id}} sa-head"/></span>
                                </a>
                            </h5>
                            </div>
                            <div id="collapse-{{$item->id}}" class="collapse {{$item->name}}" role="tabpanel" aria-labelledby="heading-{{$item->id}}">
                                <div class="card-body">
                                    <div id="accordion-{{$item->id}}">
                                        @foreach($item->subItems as $subItem)
                                        <div class="card">
                                            <div class="card-header" id="heading-{{$item->id}}-{{$subItem->id}}">
                                            <h5 class="mb-0">
                                                <a class="collapsed">
                                                {{$subItem->name}}
                                                </a>
                                                <span class="float-right"><input type="checkbox" id="foo_1" name="foo" value="1" data-select-all="sa_foo" class="checkme {{$item->name}}" data-toggle="collapse" data-target="#collapse-sub-{{$subItem->id}}"/></span>
                                            </h5>
                                            </div>
                                            <div id="collapse-sub-{{$subItem->id}}" name="foo" class="collapse {{$item->name}}" aria-labelledby="heading-{{$subItem->id}}">
                                            @foreach($subItem->itemDetails as $itemDetail)
                                                <div class="card-body">
                                                    {{$itemDetail->name}}
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
    var id = $('#userId').val();
    var span = $('#apiToken');
    var generateToken = $('#btnToken');
    var showTree = $('#btnTree');
    var treeContent = $('#treeContent');
    var bodyContent = $('#accordion');

   generateToken.click(function() {
       var token = span.html();
          $.ajax({
            url: '/api/generate_token/'+id,
            type: 'GET',
            success: function (data) {
                span.html(data.token);
            }
         });
    });

    showTree.click(function() {
        var token = span.html();
          $.ajax({
            url: '/api/items?api_token='+token,
            type: 'GET',
            statusCode: {
                401: function() {
                span.html('Please Generate Token First To Show The Tree');
                }
            },
            success: function (data) {
                console.log(data);
                if (data == '') {
                    treeContent.addClass('d-none');
                } else {
                    treeContent.removeClass('d-none');
                }
            }
         });
    });

    $(".sa-head").on("click", function(event) {
        event.stopPropagation();
    });

    $('#sa_foo_1').on('click', function(e){
        if ($(this).val() == 0) {
            $('#sa_foo_1').prop( "checked", true );
                $(this).val(1);
        } else {
            $('#sa_foo_1').prop( "checked", false );
            $(this).val(0);
        }
        $(':checkbox.SOAP').removeAttr('checked');
        $(':checkbox.SOAP').click();
    });

    $('#sa_foo_2').on('click', function(){
        if ($(this).val() == 0) {
            $('#sa_foo_2').prop( "checked", true );
                $(this).val(1);
        } else {
            $('#sa_foo_2').prop( "checked", false );
            $(this).val(0);
        }
        $(':checkbox.TOOTHPASTE').removeAttr('checked');
        $(':checkbox.TOOTHPASTE').click();
    });

    $('#sa_foo_3').on('click', function(){
        if ($(this).val() == 0) {
            $('#sa_foo_3').prop( "checked", true );
                $(this).val(1);
        } else {
            $('#sa_foo_3').prop( "checked", false );
            $(this).val(0);
        }
        $(':checkbox.SHAMPOO').removeAttr('checked');
        $(':checkbox.SHAMPOO').click();
    });

</script>
@endsection
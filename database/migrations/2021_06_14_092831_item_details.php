<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ItemDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_details', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('sub_items_id');
        });

        $data = [
            ['name'=>'GREEN SAFEGUARD', 'sub_items_id' => 1,],
            ['name'=>'RED SAFEGUARD', 'sub_items_id' => 1,],
            ['name'=>'BLUE SAFEGUARD', 'sub_items_id' => 1,],
            ['name'=>'RED DOVE', 'sub_items_id' => 2,],
            ['name'=>'BLUE DOVE', 'sub_items_id' => 2,],
            ['name'=>'RED COLGATE', 'sub_items_id' => 3,],
            ['name'=>'BLUE COLGATE', 'sub_items_id' => 3,],
            ['name'=>'RED CLOSE UP', 'sub_items_id' => 4,],
            ['name'=>'BLUE CLOSE UP', 'sub_items_id' => 4,],
            ['name'=>'RED PANTENE', 'sub_items_id' => 5,],
            ['name'=>'BLUE PANTENE', 'sub_items_id' => 5,],
            ['name'=>'RED SUNSILK', 'sub_items_id' => 6,],
            ['name'=>'BLUE SUNSILK', 'sub_items_id' => 6,],

        ];

        DB::table('item_details')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_details');
    }
}
